import React from 'react'

const Weather = ({description, city, country, error, temperature}) => {

    function matchValues () {
    if(description) {
        const weatherDescription = description.split(' ')
        const keyWords = ['cloudy','clouds', 'cloud', 'overcast', 'clear sky', 'clear']
        for(let i = 0; i < weatherDescription.length; i++) {
            if(keyWords.includes(weatherDescription[i])) {
                return <div class="icon sunny">
                <div class="sun">
                  <div class="rays"></div>
                </div>
              </div>
            }
        }
    }}

    return (

        <div className="center">
            {city && country && <p>{city}, {country}</p>}
            {temperature && <p id="temp">{Math.round(temperature)}  °C</p>}
            {/* {description && <p> Conditions: {description}</p>} */}
            {error && <p>{error}</p>}
            {description && matchValues()}
        </div>
    )
}

export default Weather; 