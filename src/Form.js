import React from 'react'

const Form = (props) => {
    return(
        <form onSubmit={props.getWeather}>
            <label htmlFor="city">Wpisz miasto</label>
            <input
            type='text'
            placeholder='Miasto'
            name='city'
            />
            <label htmlFor="country">Wpisz Państwo</label>
            <input
            type='text'
            placeholder='Pańśtwo'
            name='country'
            />
            <button>Zatwierdź</button>
        </form>
    )
}

export default Form;