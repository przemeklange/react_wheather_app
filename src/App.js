import React, {
  useState
} from 'react';
import './App.css';
import Form from './Form';
import Weather from './Weather';

function App() {
  const [weather, setWeather] = useState([])
  const APIKEY = 'cdc2f75582915308b8b4c1ea0e15371e'

  async function fetchData(e) {
    const city = e.target.elements.city.value
    const country = e.target.elements.country.value
    e.preventDefault()
    const apiData = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${APIKEY}&units=metric`)
      .then(res => res.json())
      .then(data => data)
    if (city && country) {
      setWeather({
        data: apiData,
        city: apiData.city,
        country: apiData.sys.country,
        description: apiData.weather[0].description,
        temperature: apiData.main.temp,
        error: ""
      })
    } else {
      setWeather({
        data: '',
        city: '',
        country: '',
        description: '',
        temperature: '',
        error: "Podaj proszę miejscowość lub państwo"
      })
      
    }
  }

  return ( 
    <div className = "App" >
    <Form getWeather = {
      fetchData }/> <
    Weather city = {
      weather.city
    }
    country = {
      weather.country
    }
    description = {
      weather.description
    }
    temperature = {
     weather.temperature
    }
    error = {
      weather.error
    }
    /> {
      console.log(weather.data)
    } <
    /div>
  );
}

export default App;